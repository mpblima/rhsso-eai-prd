variable "access_key"{
    type = string
}

variable "secret_key"{
    type = string
}

variable "region"{
    type = string
}

variable "instance_type"{
    type = string
}

variable "subnet_id"{
    type = string
}

variable "ssh_username"{
    type = string
}

variable "rhsso_ami_name"{
    type = string
}

variable "datagrid_ami_name"{
    type = string
}

variable "vpc_id"{
    type = string
}

variable "database_user"{
    type = string
    sensitive = true
}

variable "database_password"{
    type = string
    sensitive = true
}

variable "rhsso_load_balancer"{
    type = string
    sensitive = true
}

variable "datagrid_user"{
    type = string
    sensitive = true
}

variable "datagrid_pwd"{
    type = string
    sensitive = true
}

variable "s3_bucket_name"{
    type = string
}

variable "keystorePWD"{
    type = string
    sensitive = true
}

source "amazon-ebs" "rhsso"{
  access_key                        = "${var.access_key}"
  secret_key                        = "${var.secret_key}"
  region                            = "${var.region}"
  instance_type                     = "${var.instance_type}"
  subnet_id                         = "${var.subnet_id}"
  ssh_username                      = "${var.ssh_username}"
  ami_name                          = "${var.rhsso_ami_name}"
  vpc_id                            = "${var.vpc_id}"
  source_ami_filter {
      filters = {
          virtualization-type  = "hvm"
          name   = "RHEL-8*-x86_64-*"
          root-device-type  = "ebs"
      }
      owners  = ["309956199498"]
      most_recent = "true"
  }
}

build {
  sources = ["source.amazon-ebs.rhsso"]

  provisioner "ansible" {
    playbook_file = "../3-Ansible/rhsso.yml"
    extra_arguments = ["--extra-vars","database_user=${var.database_user} database_password=${var.database_password} rhsso_load_balancer=${var.rhsso_load_balancer} datagridUser=${var.datagrid_user} datagridPWD=${var.datagrid_pwd} keystorePWD=${var.keystorePWD}"]
  }
}
