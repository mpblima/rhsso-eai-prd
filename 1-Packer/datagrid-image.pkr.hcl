variable "access_key"{
    type = string
}

variable "secret_key"{
    type = string
}

variable "region"{
    type = string
}

variable "instance_type"{
    type = string
}

variable "subnet_id"{
    type = string
}

variable "ssh_username"{
    type = string
}

variable "datagrid_ami_name"{
    type = string
}

variable "rhsso_ami_name"{
    type = string
}

variable "vpc_id"{
    type = string
}

variable "s3_bucket_name"{
    type = string
}

variable "datagrid_user"{
    type = string
    sensitive = true
}

variable "datagrid_pwd"{
    type = string
    sensitive = true
}

packer {
  required_plugins {
    amazon = {
      version = " >= 1.0.9"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "datagrid"{
  access_key                        = "${var.access_key}"
  secret_key                        = "${var.secret_key}"
  region                            = "${var.region}"
  instance_type                     = "${var.instance_type}"
  subnet_id                         = "${var.subnet_id}"
  ssh_username                      = "${var.ssh_username}"
  ami_name                          = "${var.datagrid_ami_name}"
  vpc_id                            = "${var.vpc_id}"
  associate_public_ip_address       = true
  ssh_timeout                       = "5m"
  ssh_interface                     = "public_ip"
  #source_ami                        = "ami-0534d3148e3ce93d7"
  source_ami_filter {
      filters = {
          virtualization-type  = "hvm"
          name    = "RHEL-8.1.0_HVM-20191029-x86_64-0-Hourly2-GP2"
          root-device-type  = "ebs"
      }
      owners  = ["309956199498"]
      most_recent = "true"
  }
}

build {
  sources = ["source.amazon-ebs.datagrid"]

  provisioner "ansible" {
    playbook_file = "../3-Ansible/datagrid.yml"
    extra_arguments = ["--extra-vars", "datagrid_user=${var.datagrid_user} datagrid_pwd=${var.datagrid_pwd} secret_key=${var.secret_key} access_key=${var.access_key} region=${var.region} s3_bucket_name=${var.s3_bucket_name}"]
  }
}
